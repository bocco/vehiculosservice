package com.pandeveloper.vehicles.utils.validators;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pandeveloper.vehicles.models.Holiday;
import com.pandeveloper.vehicles.repository.HolidayRepository;

@Service
public class HolidayValidator {
	
	@Autowired
	HolidayRepository holidayRepository;
	
	public Boolean isHoliday(Date date) {
		Boolean isholiday = Boolean.TRUE;
		Holiday holiday = holidayRepository.findByDate(date);
		if(holiday==null)
			isholiday = Boolean.FALSE;
		
		return isholiday;
	}

}
