package com.pandeveloper.vehicles.utils.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlateNumberValidator {
	
	
	
	public Boolean isValidPlateNumber(String plate) {
		boolean valid = Boolean.TRUE;
		if(plate==null) {
			valid = Boolean.FALSE;
		}
		else if(plate.length()!=7) {
			valid = Boolean.FALSE;
		}
		else if(!plate.substring(0,3).matches("[A-Z]*") ||
				!plate.substring(3).matches("[0-9]*")) {
			valid = Boolean.FALSE;
		}
		
		return valid;
	}

}
