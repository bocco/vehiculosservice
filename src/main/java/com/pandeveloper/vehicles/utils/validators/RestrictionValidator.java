package com.pandeveloper.vehicles.utils.validators;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pandeveloper.vehicles.models.Holiday;
import com.pandeveloper.vehicles.models.Restriction;
import com.pandeveloper.vehicles.repository.RestrictionRepository;

@Service
public class RestrictionValidator {
	
	@Autowired
	RestrictionRepository restrictionRepository;
	
	public Boolean isRestricted(String day, String plateLastDigit,Time date) {
		List<Restriction> lRestriction = null ;
		lRestriction = restrictionRepository.findRestrictionOnDayAndTime(day, 
				plateLastDigit, 
				date);
		if(lRestriction==null || lRestriction.isEmpty()) {
			return Boolean.FALSE;
		}
		else {
			return Boolean.TRUE;
		}
	}

}
