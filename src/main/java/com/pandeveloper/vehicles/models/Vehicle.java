package com.pandeveloper.vehicles.models;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="vehicle")
@NamedQuery(name="Vehicle.findAll", query="SELECT c FROM Vehicle c")
public class Vehicle implements Serializable {

	/**
	 * Serial Uid
	 */
	private static final long serialVersionUID = -8938439618667781962L;
	
	@Id
	@Column(name = "id_vehicle", columnDefinition = "serial")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idVehicle;
	
	private String brand;
	
	private String color;
	
	private String model;
	
	@Column(name = "plate_number")
	private String plateNumber;
	
	@Column(name = "chassis_number")
	private String chassisNumber;
	
	public Integer getIdVehicle() {
		return idVehicle;
	}

	public void setIdVehicle(Integer idVehicle) {
		this.idVehicle = idVehicle;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public String getChassisNumber() {
		return chassisNumber;
	}

	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}

	

}
