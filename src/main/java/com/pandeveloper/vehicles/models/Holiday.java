package com.pandeveloper.vehicles.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="holiday")
@NamedQuery(name="Holiday.findAll", query="SELECT c FROM Holiday c")
public class Holiday implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1479069627851892855L;

	@Id
	@Column(name = "id_holiday")
	private Integer idHoliday;
	
	@Column(name = "date")
	private Date date;

	public Integer getIdHoliday() {
		return idHoliday;
	}

	public void setIdHoliday(Integer idHoliday) {
		this.idHoliday = idHoliday;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
}
