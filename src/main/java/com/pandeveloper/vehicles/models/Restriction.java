package com.pandeveloper.vehicles.models;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.*;

@Entity
@Table(name="restriction")
@NamedQuery(name="Restriction.findAll", query="SELECT c FROM Restriction c")
public class Restriction implements Serializable {

	
	
	/**
	 * Serial Uid
	 */
	private static final long serialVersionUID = -4717728889086692131L;

	@Id
	@Column(name = "id_restriction")
	private Integer idRestriction;
	
	@Column(name = "restriction_number")
	private String restrictionPlateNumber;
	
	@Column(name = "day_of_week")
	private String dayOfWeek;
	
	@Column(name = "restriction_begin_time")
	private Time restrictionBeginTime;
	
	@Column(name = "restriction_end_time")
	private Time restrictionEndTime;

	public Integer getIdRestriction() {
		return idRestriction;
	}

	public void setIdRestriction(Integer idRestriction) {
		this.idRestriction = idRestriction;
	}

	public String getRestrictionPlateNumber() {
		return restrictionPlateNumber;
	}

	public void setRestrictionPlateNumber(String restrictionPlateNumber) {
		this.restrictionPlateNumber = restrictionPlateNumber;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Time getRestrictionBeginTime() {
		return restrictionBeginTime;
	}

	public void setRestrictionBeginTime(Time restrictionBeginTime) {
		this.restrictionBeginTime = restrictionBeginTime;
	}

	public Time getRestrictionEndTime() {
		return restrictionEndTime;
	}

	public void setRestrictionEndTime(Time restrictionEndTime) {
		this.restrictionEndTime = restrictionEndTime;
	}
	
	
}
