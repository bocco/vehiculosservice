package com.pandeveloper.vehicles.exception;

import java.util.Date;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

	  @ExceptionHandler(ResourceNotFoundException.class)
	    public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
	         ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
	         return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	    }

	  @ExceptionHandler(Exception.class)
	    public ResponseEntity<?> globleExceptionHandler(Exception ex, WebRequest request) {
	        ex.printStackTrace();
	    	ErrorDetails errorDetails = new ErrorDetails(new Date(), "Ha ocurrido un problema con el servidor. Intente mas tarde.", request.getDescription(false));
	        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  
	 
	  @ExceptionHandler(RequestRejectedException.class)
	    public ResponseEntity<?> requestRejectedExceptionHandler(RequestRejectedException ex, WebRequest request) {
	        ex.printStackTrace();
	    	ErrorDetails errorDetails = new ErrorDetails(new Date(), "La solicitud ha sido rechazada", "La solicitud ha sido rechazada");
	        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	    }
	  
	  
	  @ExceptionHandler(DataIntegrityViolationException.class)
	    public ResponseEntity<?> dataIntegrityViolationExceptionHandler(DataIntegrityViolationException ex, WebRequest request) {
		  if(ex.getCause() instanceof ConstraintViolationException) {
		  ConstraintViolationException constraint = (ConstraintViolationException) ex.getCause();
			if(constraint.getConstraintName().equals("vehicle_plate_number_key")) {
				ErrorDetails err = new ErrorDetails(new Date(), "El numero de placa ya ha sido ingresado", "El número de placa ya ha sido ingresado");
				return ResponseEntity.status(HttpStatus.CONFLICT).body(err);
			}
			if(constraint.getConstraintName().equals("vehicle_chassis_number_key")) {
				ErrorDetails err = new ErrorDetails(new Date(), "El numero de chasis ya ha sido ingresado", "El número de placa chasis ya ha sido ingresado.");
				return ResponseEntity.status(HttpStatus.CONFLICT).body(err);
			}
			
		  }
		  
		  ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		  return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDetails);
	    }
}
