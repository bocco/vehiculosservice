package com.pandeveloper.vehicles.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pandeveloper.vehicles.models.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {
	
	Vehicle findByPlateNumber(String plateNumber);

}
