package com.pandeveloper.vehicles.repository;

import java.sql.Time;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pandeveloper.vehicles.models.Restriction;

public interface RestrictionRepository extends JpaRepository<Restriction, Integer> {
	
	@Query("select c from Restriction c where c.dayOfWeek = ?1 and c.restrictionPlateNumber = ?2 "
			+ "and ?3 between c.restrictionBeginTime and c.restrictionEndTime")
	public List<Restriction> findRestrictionOnDayAndTime(String dayOfWeek, String restrictionNumber,Time hour);

}
