package com.pandeveloper.vehicles.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pandeveloper.vehicles.models.Holiday;

public interface HolidayRepository extends JpaRepository<Holiday, Integer> {	
	Holiday findByDate(Date date);
}
