package com.pandeveloper.vehicles.controllers;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pandeveloper.vehicles.exception.ErrorDetails;
import com.pandeveloper.vehicles.models.Restriction;
import com.pandeveloper.vehicles.repository.RestrictionRepository;
import com.pandeveloper.vehicles.utils.validators.HolidayValidator;
import com.pandeveloper.vehicles.utils.validators.PlateNumberValidator;
import com.pandeveloper.vehicles.utils.validators.RestrictionValidator;

@RestController
@RequestMapping("/restriction")
public class RestrictionController {
	
	@Autowired
	RestrictionRepository restrictionRepository;
	
	@Autowired
	PlateNumberValidator plateNumberValidator;
	
	@Autowired
	HolidayValidator holidayValidator;
	
	@Autowired
	RestrictionValidator restrictionValidator;
	
	@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
			RequestMethod.DELETE, RequestMethod.OPTIONS })
	@GetMapping("/plate/{plateNumber}/date/{date}")
	public ResponseEntity<?> isVehiclePlateNumberRestricted(@PathVariable(name = "plateNumber") String plateNumber, 
															@PathVariable(name = "date") String date
															){
		if(!(plateNumberValidator.isValidPlateNumber(plateNumber))) {
			ErrorDetails err = new ErrorDetails(new Date(), "Número de placa incorrecto", "La placa debe contener el formato AAA####");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
		}
		
	
		DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date formattedTime = null;
		try {
			formattedTime = new Date(dateFormatter.parse(date).getTime());
			Date holidayTime = formattedTime;
			if(holidayValidator.isHoliday(formattedTime)) {
				return ResponseEntity.ok().body(Boolean.FALSE);
			}
			
			Boolean restricted = restrictionValidator.isRestricted(String.valueOf(formattedTime.getDay()), 
						plateNumber.substring(plateNumber.length()-1), 
						new Time(formattedTime.getTime()));
			
			return ResponseEntity.ok().body(restricted);
			
		} catch (ParseException e) {
			ErrorDetails err = new ErrorDetails(new Date(), "Error en conversión de fecha", "No se pudo converir "+date+" a fecha.");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
		}
		
	}

}
