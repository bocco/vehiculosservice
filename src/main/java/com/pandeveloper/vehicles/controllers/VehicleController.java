package com.pandeveloper.vehicles.controllers;

import java.util.Date;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pandeveloper.vehicles.exception.ErrorDetails;
import com.pandeveloper.vehicles.models.Vehicle;
import com.pandeveloper.vehicles.repository.VehicleRepository;
import com.pandeveloper.vehicles.utils.validators.PlateNumberValidator;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {

	@Autowired
	private VehicleRepository vehicleRespository;
	
	@Autowired
	private PlateNumberValidator plateNumberValidator;
	
	@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
			RequestMethod.DELETE, RequestMethod.OPTIONS })
	@GetMapping("/plateNumber/{plateNumber}")
	public ResponseEntity<?> vehiclePlateNumberExist(@PathVariable(name="plateNumber") String plateNumber){
		Vehicle vehicle = vehicleRespository.findByPlateNumber(plateNumber);
		return ResponseEntity.ok().body(vehicle);
		
	}
	
	@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
			RequestMethod.DELETE, RequestMethod.OPTIONS })
	@PostMapping("/save")
	public ResponseEntity<?> saveVehicle(@RequestBody Vehicle vehicle){
		if(!(plateNumberValidator.isValidPlateNumber(vehicle.getPlateNumber()))) {
			ErrorDetails err = new ErrorDetails(new Date(), "Número de placa incorrecto", "Número de placa incorrecto");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
		}
		
		if(vehicle.getBrand()==null || vehicle.getBrand().equals("")) {
			ErrorDetails err = new ErrorDetails(new Date(), "El atributo marca es obligatorio", "El atributo marca es obligatorio");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
		}
		
		if(vehicle.getChassisNumber()==null || vehicle.getChassisNumber().equals("")) {
			ErrorDetails err = new ErrorDetails(new Date(), "El atributo número de chasis es obligatorio", "El atributo número de chasis es obligatorio");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
		} 
		
		if( vehicle.getChassisNumber().length()!=17) {
			ErrorDetails err = new ErrorDetails(new Date(), "El atributo número de chasis debe contener 17 caracteres", "El atributo número de chasis debe contener 17 caracteres");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
		} 
		
		if(vehicle.getColor()==null || vehicle.getColor().equals("")) {
			ErrorDetails err = new ErrorDetails(new Date(), "El atributo color es obligatorio", "El atributo color es obligatorio");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
		}
		
		vehicleRespository.save(vehicle);
		return ResponseEntity.status(HttpStatus.CREATED).build();
		
	}
	
	
}
