package com.pandeveloper.vehicles;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Time;
import java.util.Calendar;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.pandeveloper.vehicles.utils.validators.HolidayValidator;
import com.pandeveloper.vehicles.utils.validators.PlateNumberValidator;
import com.pandeveloper.vehicles.utils.validators.RestrictionValidator;

@SpringBootTest
class VehiclesApplicationTests {
	
	@Autowired
	HolidayValidator holidayValidator;
	
	@Autowired
	RestrictionValidator restrictionValidator;
	
	@Test
	void plateTestPositive() {
		PlateNumberValidator plateValidator = new PlateNumberValidator();
		Boolean test= plateValidator.isValidPlateNumber("PVC6543");
		assertEquals(Boolean.TRUE, test);
	}
	
	@Test
	void plateTestNegativeNot7Lenght() {
		PlateNumberValidator plateValidator = new PlateNumberValidator();
		Boolean test= plateValidator.isValidPlateNumber("PV6543");
		assertEquals(Boolean.FALSE, test);
	}
	
	@Test
	void plateTestNegativeNumberOnLetters() {
		PlateNumberValidator plateValidator = new PlateNumberValidator();
		Boolean test= plateValidator.isValidPlateNumber("PV66543");
		assertEquals(Boolean.FALSE, test);
	}
	
	@Test
	void plateTestNegativeLetterOnNumbers() {
		PlateNumberValidator plateValidator = new PlateNumberValidator();
		Boolean test= plateValidator.isValidPlateNumber("PVC6A43");
		assertEquals(Boolean.FALSE, test);
	}
	
	@Test
	void plateTestNegativeNull() {
		PlateNumberValidator plateValidator = new PlateNumberValidator();
		Boolean test= plateValidator.isValidPlateNumber(null);
		assertEquals(Boolean.FALSE, test);
	}
	
	/*
	 * As precondition an entry on table holiday must be created
	 */
	@Test
	void holidayPositive() {
		Calendar c1 = Calendar.getInstance();  
        c1.set(2022, 4, 23, 0, 0, 0);
		Boolean test= holidayValidator.isHoliday(c1.getTime());
		assertEquals(Boolean.TRUE, test);
	}
	
	@Test
	void holidayNegative() {
		Calendar c1 = Calendar.getInstance();  
        c1.set(Calendar.MONTH, 05);
        c1.set(Calendar.DATE, 25);
        c1.set(Calendar.YEAR, 2022);
		Boolean test= holidayValidator.isHoliday(c1.getTime());
		assertEquals(Boolean.FALSE, test);
	}
	
	/*
	 * As precondition an entry on table restriction must be created
	 */
	@Test
	void restrictedPositive() {
		Boolean test= restrictionValidator.isRestricted("1", "1", new Time(10, 0, 0));
		assertEquals(Boolean.TRUE, test);
	}
	
	/*
	 * As precondition an entry on table restriction must be created
	 */
	@Test
	void restrictedNegative() {
		Boolean test= restrictionValidator.isRestricted("1", "3", new Time(10, 0, 0));
		assertEquals(Boolean.FALSE, test);
	}

}
