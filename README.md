# Backend spring boot proyecto vehiculos y restriccion de movilidad
Repositorio del backend del proyecto de vehículos y restricción de movilidad.
puede probarse en : https://vehicleservicetest.herokuapp.com/
## Instalación en ambiente local
1.- Usa una base de datos Postgres 14. Los datos de la conexión se enceuntran en aplication.properties. Se puede cargar un respaldo en un ambiente propio. La base se encuentra en la nube.

2.-Se puede abrir el proyecto usando el Spring tool suite, automáticamente descargará las dependencias. Una vez realizado esto se puede correr desde el aplicativo 
## Descripción de tablas de la BDD
- vehicle: En esta tabla se almacena  la información de vehículos.
- restriction: En esta tabla se almacena los horarios de la restrición de movilidad y el número de placa que se encuentra ligados a esos horarios y el día de la semana que representa.
- holiday: En esta tabla se almacenan fechas de días festivos en los cuales nos e aplica la restricción de movilidad vehicular.

## Autorización
El backend usa autorizacion básica. Los datos pueden encontrarse dentro de la clase SecurityConfig.

## Descripción de endpoints
Tiene los siguientes endpoints para consulta y guardado de vehículo:
- POST `/vehicle/save` para guardado de información
- GET `/vehicle/plateNumber/{plateNumber}` para consulta de vehiculos

Para la consulta de la restricción de movilidad se tiene el siguiente endpoint:
- GET `/restriction/plate/{plateNumber}/date/{date}`
